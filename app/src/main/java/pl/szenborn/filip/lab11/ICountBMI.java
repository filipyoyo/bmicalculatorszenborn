package pl.szenborn.filip.lab11;

/**
 * Created by Filip on 20.03.2017.
 */

public interface ICountBMI {
    public boolean isValidMass(float mass);
    public boolean isValidHeight(float height);
    public float countBmi(float mass, float height);
}

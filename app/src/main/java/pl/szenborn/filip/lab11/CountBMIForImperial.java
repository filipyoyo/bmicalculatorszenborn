package pl.szenborn.filip.lab11;

/**
 * Created by Filip on 20.03.2017.
 */

public class CountBMIForImperial implements ICountBMI {

    static final float minMass=10f*(1 / 0.4536f);
    static final float maxMass=250f*(1 / 0.4536f);
    static final float minHeight=0.5f*(1 / 0.254f)*10;
    static final float maxHeight=2.5f*(1 / 0.254f)*10;
    @Override
    public boolean isValidMass(float mass) {
        return mass>minMass && mass < maxMass;
    }

    @Override
    public boolean isValidHeight(float height) {
        return height>minHeight && height<maxHeight;
    }

    @Override
    public float countBmi(float mass, float height) {
        if (!isValidHeight(height))
            throw new IllegalArgumentException("Zła wysokość");
        if (!isValidMass(mass))
            throw new IllegalArgumentException("Zła waga");
        return (mass/(height*height))*703f;
    }
}

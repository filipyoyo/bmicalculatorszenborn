package pl.szenborn.filip.lab11;

/**
 * Created by Filip on 20.03.2017.
 */

public class CountBMIForMetric implements ICountBMI {

    static final float minMass=10f;
    static final float maxMass=250f;
    static final float minHeight=0.5f;
    static final float maxHeight=2.5f;
    @Override
    public boolean isValidMass(float mass) {

        return mass>minMass && mass < maxMass;
    }

    @Override
    public boolean isValidHeight(float height) {
        return height>minHeight && height<maxHeight;
    }

    @Override
    public float countBmi(float mass, float height) {
        if (!isValidHeight(height))
            throw new IllegalArgumentException("Zła wysokość");
        if (!isValidMass(mass))
            throw new IllegalArgumentException("Zła waga");
        return mass/(height*height);
    }
}

package pl.szenborn.filip.lab11;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.buttonCount)
    Button btn;
    @BindView(R.id.editHeigth)
    EditText editH;
    @BindView(R.id.editMass)
    EditText editM;
    @BindView(R.id.showBMI)
    TextView viewResult;
    @BindView(R.id.labelHUnit)
    TextView labelHUnit;
    @BindView(R.id.labelMUnit)
    TextView labelMUnit;
    @BindView(R.id.showBMIdescription)
    TextView viewResultDescription;
    @BindView(R.id.radioImperial)
    RadioButton radioImperial;

    boolean metric = true;
    private View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                renderBMIResult(Float.parseFloat(editM.getText().toString()), Float.parseFloat(editH.getText().toString()));
            } catch (IllegalArgumentException e) {
                Toast.makeText(getApplicationContext(), R.string.badInput, Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        btn.setOnClickListener(btnClick);
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        metric = (sharedPref.getBoolean(getString(R.string.saved_unit), true));
        setMetric(metric);
        editH.setText(sharedPref.getString(getString(R.string.saved_height), ""));
        editM.setText(sharedPref.getString(getString(R.string.saved_mass), ""));
        radioImperial.setChecked(!metric);
        try {
            renderBMIResult(Float.parseFloat(editM.getText().toString()), Float.parseFloat(editH.getText().toString()));
        } catch (Exception e) {
        }
    }

    public void renderBMIResult(float m, float h) throws IllegalArgumentException {

        ICountBMI countBMI;
        if (metric)
            countBMI = new CountBMIForMetric();
        else
            countBMI = new CountBMIForImperial();

        float bmi = countBMI.countBmi(m, h);
        viewResult.setText(String.format("%.1f", bmi));
        if (bmi < 16f) {
            viewResult.setTextColor(getResources().getColor(R.color.colorAlert));
            viewResultDescription.setText(R.string.BMIdesc1);
        } else if (bmi < 17f) {
            viewResult.setTextColor(getResources().getColor(R.color.colorAlert));
            viewResultDescription.setText(R.string.BMIdesc2);
        } else if (bmi < 18.5f) {
            viewResult.setTextColor(getResources().getColor(R.color.colorWarn));
            viewResultDescription.setText(R.string.BMIdesc3);
        } else if (bmi < 25) {
            viewResult.setTextColor(getResources().getColor(R.color.colorOK));
            viewResultDescription.setText(R.string.BMIdesc4);
        } else if (bmi < 30) {
            viewResult.setTextColor(getResources().getColor(R.color.colorWarn));
            viewResultDescription.setText(R.string.BMIdesc5);
        } else if (bmi < 35) {
            viewResult.setTextColor(getResources().getColor(R.color.colorAlert));
            viewResultDescription.setText(R.string.BMIdesc6);
        } else if (bmi < 40) {
            viewResult.setTextColor(getResources().getColor(R.color.colorAlert));
            viewResultDescription.setText(R.string.BMIdesc7);
        } else {
            viewResult.setTextColor(getResources().getColor(R.color.colorAlert));
            viewResultDescription.setText(R.string.BMIdesc8);
        }
        hideSoftKeyboard(MainActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSave:
                SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.saved_height), editH.getText().toString());
                editor.putString(getString(R.string.saved_mass), editM.getText().toString());
                editor.putBoolean(getString(R.string.saved_unit), metric);
                editor.commit();
                return true;
            case R.id.menuShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, R.string.BMIshareResult + viewResult.getText().toString() + R.string.BMIshare2 + viewResultDescription.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
            case R.id.menuAbout:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setMetric(boolean new_metric) {
        if (new_metric) {
            labelHUnit.setText(R.string.hUnitMetric);
            labelMUnit.setText(R.string.mUnitMetric);
            if (!metric) {
                metric = true;
                editH.setText(String.format("%.2f", Float.parseFloat(editH.getText().toString()) * 0.0254f));
                editM.setText(String.format("%.2f", Float.parseFloat(editM.getText().toString()) * 0.4536f));
            }
        } else {
            labelHUnit.setText(R.string.hUnitImperial);
            labelMUnit.setText(R.string.mUnitImperial);
            if (metric) {
                metric = false;
                editH.setText(String.format("%.2f", Float.parseFloat(editH.getText().toString()) * (1 / 0.0254f)));
                editM.setText(String.format("%.2f", Float.parseFloat(editM.getText().toString()) * (1 / 0.4536f)));
            }
        }
    }

    public void onRadioButtonClicked(View view) {
        if (((RadioButton) view).isChecked()) setMetric(view.getId() == R.id.radioMetric);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}

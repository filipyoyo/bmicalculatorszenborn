package pl.szenborn.filip.lab11;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

/**
 * Created by Filip on 20.03.2017.
 */

public class CountBMITest {


    @Test
    public void massUnderZero_isInvalid() throws Exception {
        //given
        float testMass=-1.0f;
        //when
        ICountBMI ins = new CountBMIForMetric();
        //then
        boolean actual = ins.isValidMass(testMass);
        assertFalse(actual);
    }
    public void countBmi_isInvalid() throws Exception {
        //given
        float testMass=70f,testHeight=1.8f;
        //when
        ICountBMI ins = new CountBMIForMetric();
        //then
        float actual = ins.countBmi(testMass,testHeight);
        assertEquals(actual,21.6,0.01);
    }
}
